#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Party Open Items',
    'name_de_DE': 'Buchhaltung Parteien Offene-Posten-Liste',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.tryton.org/',
    'description': '''Open-Items-List for Parties
    - Provides an open item list for invoices.
''',
    'description_de_DE': '''OPOS-Liste für Parteien
    - Stellt eine Offene-Posten-Liste für Parteien zur Verfügung.
''',
    'depends': [
        'company',
        'party',
        'account_product',
        'account_move_invoice',
        'account_move_external_reference',
        'account_invoice_external_reference',
        'account_option_party_required',
    ],
    'xml': [
        'account.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ]
}
