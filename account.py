#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.wizard import Wizard
from trytond.pool import Pool


class OpenItemListAsk(ModelView):
    'Open Item List Ask'
    _name = 'account.open_item_list.init'
    _description = __doc__

    period = fields.Many2One('account.period', 'Period', required=False)
    report_date = fields.Date('Date', required=True)
    parties = fields.Many2Many('party.party', None, None, 'Parties')
    accounts = fields.Many2Many('account.account', None, None, 'Accounts',
            domain=[('reconcile', '=', True)])
    reconciled = fields.Boolean('Include reconciled items')
    sort_order = fields.Selection(
            [('sort_party_name', 'by party name'),
             ('sort_party_code', 'by party code')],
            "Sort Order", required=True)

    def default_accounts(self):
        account_obj = Pool().get('account.account')
        account_ids = account_obj.search([
            ('reconcile', '=', True),
            ('party_is_mandatory', '=', True),
            ])
        return account_ids

    def default_report_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

    def default_sort_order(self):
        return 'sort_party_code'

OpenItemListAsk()


class PrintOpenItemListReport(Wizard):
    'Print Open Item List Report'
    _name = 'account.wizard_open_item_list'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.open_item_list.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('print', 'Print', 'tryton-ok', True),
                ],
            },
        },
        'print': {
            'result': {
                'type': 'print',
                'report': 'account.open_item_list_report',
                'state': 'end',
            },
        },
    }

PrintOpenItemListReport()


class OpenItemAccountAsk(ModelView):
    'Open Item Account Ask'
    _name = 'account.open_item_account.init'
    _description = __doc__

    period = fields.Many2One('account.period', 'Period', required=False)
    report_date = fields.Date('Date', required=True)
    parties = fields.Many2Many('party.party', None, None, 'Parties')
    reconciled = fields.Boolean('Include reconciled items')
    notes = fields.Text('Notes')
    sort_order = fields.Selection(
            [('sort_party_name', 'by party name'),
             ('sort_party_code', 'by party code')],
            "Sort Order", required=True)

    def default_report_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

    def default_sort_order(self):
        return 'sort_party_code'

OpenItemAccountAsk()


class PrintOpenItemAccountReport(Wizard):
    'Print Open Item Account Report'
    _name = 'account.wizard_open_item_account'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.open_item_account.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('print', 'Print', 'tryton-ok', True),
                ],
            },
        },
        'print': {
            'result': {
                'type': 'print',
                'report': 'account.open_item_account_report',
                'state': 'end',
            },
        },
    }

PrintOpenItemAccountReport()


class OpenItemReport(Report):
    _name = 'account.open_open_item_report'

    def parse(self, report, objects, datas, localcontext=None):
        pool = Pool()
        move_line_obj = pool.get('account.move.line')
        account_obj = pool.get('account.account')
        fiscalyear_obj = pool.get('account.fiscalyear')

        fiscalyear_ids = fiscalyear_obj.search([], limit=1,
            order=[('start_date', 'ASC')])
        if fiscalyear_ids:
            first_fiscalyear_id = fiscalyear_ids[0]

        # Initialize and map general variables to localcontext dict
        base_account_ids = None
        if 'accounts' in datas['form'] and datas['form']['accounts'][0][1]:
            base_account_ids = datas['form']['accounts'][0][1]
        account_ids = account_obj._get_accounts_report_open_items(
                base_account_ids)
        if not base_account_ids:
            base_account_ids = account_ids
        localcontext['accounts'] = account_obj.browse(base_account_ids)
        if 'notes' in datas['form']:
            localcontext['notes'] =  datas['form']['notes'] and \
                    datas['form']['notes'].decode('utf-8')
        localcontext['report_date'] = datas['form']['report_date']
        localcontext['total_debit'] = Decimal('0.0')
        localcontext['total_credit'] = Decimal('0.0')
        localcontext['total_balance'] = Decimal('0.0')
        # Build up search query list
        move_line_query_list = [
            ('state', '=', 'valid'),
                ['OR',
                    ('move.journal.type', '!=', 'situation'),
                    ('move.period.fiscalyear', '=', first_fiscalyear_id),
                ]
            ]
        if 'reconciled' in datas['form'] and datas['form']['reconciled']:
            pass
        else:
            move_line_query_list.append(('reconciliation', '=', False))
        if 'report_date' in datas['form']:
            move_line_query_list.append(
                ('date', '<=', datas['form']['report_date']))
        if 'period' in datas['form'] and datas['form']['period']:
            move_line_query_list.append(
                ('period', '=', datas['form']['period']))
        if account_ids:
            move_line_query_list.append(('account', 'in', account_ids))
        if datas['form']['parties'] and datas['form']['parties'][0][1]:
            move_line_query_list.append(
                ('party', 'in', datas['form']['parties'][0][1]))
        # Search and get move lines objects
        move_line_ids = move_line_obj.search(move_line_query_list)
        move_lines = move_line_obj.browse(move_line_ids)

        def _sort_party_code(a):
            if a.party and 'code' in a.party:
                return a.party.code
            return 0

        def _sort_party_name(a):
            if a.party and 'name' in a.party:
                return a.party.name
            return 0

        # Sort parties
        _sort_order = 'sort_order' in datas['form'] \
                and datas['form']['sort_order'] or "sort_party_code"
        if _sort_order == 'sort_party_name':
            move_lines.sort(key=_sort_party_name, reverse=True)
        else:
            move_lines.sort(key=_sort_party_code, reverse=True)
        localcontext['sort_order'] = _sort_order

        # instanciate OpenItem object
        open_item_list = OpenItem()

        while True:
            if move_lines:
                move_line = move_lines.pop()
            else:
                break

            if move_line is None:
                continue

            # Create new record for party or None party, if not exist
            if not move_line.party:
                if None not in [r['party'] for r in open_item_list.report]:
                    open_item_list.create_new_party_record(move_line)
            else:
                if move_line.party not in [
                        record['party'] for record in open_item_list.report
                        if record['party'] is not None]:
                    open_item_list.create_new_party_record(move_line)

            # Find dict index in the list of dicts to sum the totals
            open_item_list.index = -1
            none_party_index = -1
            for i, record in enumerate(open_item_list.report):
                if record['party'] and record['party'] == move_line.party:
                    # For move lines with parties, use the record with
                    #   party name
                    open_item_list.index = i
                    break
                if not record['party']:
                    # For move lines without party, use the record with
                    #   None party name
                    none_party_index = i

            if open_item_list.index == -1 and none_party_index != -1:
                open_item_list.index = none_party_index

            if 'invoice' in move_line.move and move_line.move.invoice:
                invoice = move_line.move.invoice[0]
                open_item_list.add_invoice(invoice)
                # Check if invoice lines in movelines
                for item in invoice.lines_to_pay:
                    if item in move_lines:
                        move_lines.remove(item)
                for item in invoice.payment_lines:
                    if item in move_lines:
                        move_lines.remove(item)

            elif move_line.reconciliation:
                open_item_list.add_reconciliation(move_line.reconciliation)
            else:
                open_item_list.add_move_line(move_line)
            continue
        localcontext['total_debit'] = open_item_list.total_debit
        localcontext['total_credit'] = open_item_list.total_credit
        localcontext['total_balance'] = open_item_list.total_balance
        objects = open_item_list.report
        res = super(OpenItemReport, self).parse(report, objects, datas,
                localcontext)

        return res

OpenItemReport()


class OpenItemListReport(OpenItemReport):
    _name = 'account.open_item_list_report'

OpenItemListReport()


class OpenItemAccountReport(OpenItemReport):
    _name = 'account.open_item_account_report'

OpenItemAccountReport()


class OpenItem(object):
    ''' Container class for collect move lines as open items.
    '''
    def __init__(self):
        self.report = []
        self.index = -1
        self.total_debit = Decimal('0.0')
        self.total_credit = Decimal('0.0')
        self.total_balance = Decimal('0.0')

    def _total(self, move_lines=[]):
        debit = Decimal('0.0')
        credit = Decimal('0.0')
        for line in move_lines:
            debit += line.debit
            credit += line.credit
        self.report[self.index]['debit'] += debit
        self.total_debit += debit
        self.report[self.index]['credit'] += credit
        self.total_credit += credit
        self.report[self.index]['balance'] += debit - credit
        self.total_balance += debit - credit
        return (debit, credit)

    def create_new_party_record(self, move_line):
        # Find out if there is an invoice address, else take addresses[0]
        default_address = None
        if move_line.party:
            for address in move_line.party.addresses:
                if hasattr(address, 'invoice') and address.invoice:
                    default_address = address

            if not default_address and move_line.party.addresses:
                default_address = move_line.party.addresses[0]

        self.report.append({
                'party': move_line.party or None,
                'default_address': default_address,
                'debit': Decimal('0.0'),
                'credit': Decimal('0.0'),
                'balance': Decimal('0.0'),
                'unreconciled': {
                    'invoices': [],
                    'invoice_debits': [],
                    'invoice_credits': [],
                    'invoice_balances': [],
                    'other_lines_to_pay': [],
                    'other_lines_paid': [],
                    'other_lines_to_pay_debit': Decimal('0.0'),
                    'other_lines_to_pay_credit': Decimal('0.0'),
                    'other_lines_to_pay_balance': Decimal('0.0'),
                    'other_lines_paid_debit': Decimal('0.0'),
                    'other_lines_paid_credit': Decimal('0.0'),
                    'other_lines_paid_balance': Decimal('0.0'),},
                'reconciled':   {
                    'invoices': [],
                    'invoice_debits': [],
                    'invoice_credits': [],
                    'invoice_balances': [],
                    'others': [],
                    'others_debits': [],
                    'others_credits': [],
                    'others_balances': [],
                    },})

    def add_invoice(self, invoice):
        # item is an invoice
        if invoice.state == "open":
            node = self.report[self.index]['unreconciled']
            if invoice in node['invoices']:
                return
            node['invoices'].append(invoice)

            (debit, credit) = \
                    self._total(invoice.lines_to_pay \
                    + invoice.payment_lines)

            node['invoice_debits'].append(debit)
            node['invoice_credits'].append(credit)
            node['invoice_balances'].append(debit - credit)

        elif invoice.state =="paid":
            node = self.report[self.index]['reconciled']
            if invoice in node['invoices']:
                return
            node['invoices'].append(invoice)

            (debit, credit) = \
                    self._total(invoice.lines_to_pay \
                    + invoice.payment_lines)
            node['invoice_debits'].append(debit)
            node['invoice_credits'].append(credit)
            node['invoice_balances'].append(debit - credit)
        else:
            raise AssertionError, "Invoices in this Report must be " \
                    "of state 'open' or 'paid' but not %s" % (invoice.state,)

    def add_reconciliation(self, reconciliation):
        # line is a reconciled move line
        node = self.report[self.index]['reconciled']
        if reconciliation in [item for item in node['others']]:
            return
        else:
            node['others'].append(reconciliation)

        (debit, credit) = self._total(reconciliation.lines)
        node['others_debits'].append(debit)
        node['others_credits'].append(credit)
        node['others_balances'].append(debit - credit)

    def add_move_line(self, line):
        # item is not an unreconciled move line
        node = self.report[self.index]['unreconciled']
        if line in node['other_lines_paid'] + node['other_lines_to_pay']:
                return
        if line.journal.type == 'cash':
            # payment line
            node['other_lines_paid'].append(line)
            (debit, credit) = self._total([line])
            node['other_lines_paid_debit'] += debit
            node['other_lines_paid_credit'] += credit
            node['other_lines_paid_balance'] += debit - credit
        else:
            # line to pay
            node['other_lines_to_pay'].append(line)
            (debit, credit) = self._total([line])
            node['other_lines_to_pay_debit'] += debit
            node['other_lines_to_pay_credit'] += credit
            node['other_lines_to_pay_balance'] += debit - credit


class Account(ModelSQL, ModelView):
    _name = 'account.account'

    def _get_accounts_report_open_items(self, account_ids=None):
        if account_ids:
            res = account_ids
        else:
            res = self.search([('reconcile', '=', True)])
        return res

Account()
